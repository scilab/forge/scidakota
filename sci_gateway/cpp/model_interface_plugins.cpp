#include <iostream>

#include "ParallelLibrary.H"
#include "ProblemDescDB.H"
#include "DakotaStrategy.H"
#include "DakotaModel.H"
#include "DakotaInterface.H"

#include "SciSerialDirectApplicInterface.h"
//#include "PluginParallelDirectApplicInterface.H"

/** Iterate over models and plugin appropriate interface: serial or parallel. */
void model_interface_plugins(Dakota::ProblemDescDB& problem_db)
{
  int initialized = 0;
#ifdef DAKOTA_HAVE_MPI
  MPI_Initialized(&initialized);
#endif // DAKOTA_HAVE_MPI

  // Do something to put the DB in a usable set/get state (unlock and set list
  // iterators).  The approach below is sufficient for simple input files, but
  // more advanced usage would require set_db_list_nodes() or equivalent.
  problem_db.resolve_top_method();

  if (problem_db.get_string("interface.type") != "direct") return;

  // Library mode interface plug-ins.
  // Model updates are performed on all processors.
  Dakota::ModelList& models = problem_db.model_list();
  Dakota::ModelLIter ml_iter;
  for (ml_iter = models.begin(); ml_iter != models.end(); ml_iter++)
    {
      const Dakota::StringArray& drivers = ml_iter->problem_description_db().get_dsa("interface.application.analysis_drivers");
      Dakota::Interface& interface = ml_iter->interface();
      bool scilab_plugin   = contains(drivers, "scilab");
      bool serial_plugin   = !initialized && scilab_plugin;
      bool parallel_plugin =  initialized && scilab_plugin;

      // Either case: set DB nodes to the existing Model specification
      if (serial_plugin || parallel_plugin) problem_db.set_db_model_nodes(ml_iter->model_id());
      // Serial case: plug in derived Interface object without an analysisComm
      if (serial_plugin) interface.assign_rep(new SciDirectApplicInterface(problem_db), false);
      else
	{
	  Cerr << "Error: no interface plugin performed.  Check compatibility "
	       << "between parallel configuration and selected analysis_driver."
	       << std::endl;
	  Dakota::abort_handler(-1);
	}
    }
}
