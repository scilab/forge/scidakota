// This file is released under the 3-clause BSD license. See COPYING-BSD.

TBX_GATEWAY_PATH = get_absolute_file_path("builder_gateway_cpp.sce");
Use_Dakota_51    = %F;

if Use_Dakota_51 then
  // For dakota-5.1
  // -lhopspack
  DAKOTA_PATH = '/opt/Dakota-5.1';
  LDFLAGS = '-L' + DAKOTA_PATH + '/lib';
  LDFLAGS = LDFLAGS + ' -ldakota -lconmin -lddace -linterfaces -lmomh';
  LDFLAGS = LDFLAGS + ' -lpecos -lsparsegrid -lnidr';
  LDFLAGS = LDFLAGS + ' -lcport -lfsudace -ljega -lncsuopt';
  LDFLAGS = LDFLAGS + ' -lopt -lpsuade -lsurfpack';
  LDFLAGS = LDFLAGS + ' -lcolin -llhs';
  LDFLAGS = LDFLAGS + ' -lnewmat -lscolib -lteuchos';
  LDFLAGS = LDFLAGS + ' -lboost_signals -lpython2.6';
  LDFLAGS = LDFLAGS + ' ' + DAKOTA_PATH + '/lib/libpebbl.a';
  LDFLAGS = LDFLAGS + ' ' + DAKOTA_PATH + '/lib/libutilib.a';
  LDFLAGS = LDFLAGS + ' ' + DAKOTA_PATH + '/lib/libtinyxml.a';
  LDFLAGS = LDFLAGS + ' ' + DAKOTA_PATH + '/lib/lib3po.a';
  LDFLAGS = LDFLAGS + ' ' + DAKOTA_PATH + '/lib/libamplsolver.a';
else
  // For dakota trunk
  DAKOTA_PATH = '/home/yann/Documents/Stage/repositories/Dakota/trunK';
  LDFLAGS = '-L' + DAKOTA_PATH + '/lib';
  LDFLAGS = LDFLAGS + ' -ldace -lmars -lpds -lsurfaces';
  LDFLAGS = LDFLAGS + ' -l3po -ldakota_src -lmod -lpebbl -lsurfpack';
  LDFLAGS = LDFLAGS + ' -lamplsolver -ldfftpack -lmods -lpecos -lteuchos';
  LDFLAGS = LDFLAGS + ' -lanalyzer -leutils -lmoga -lpecos_src -ltinyxml';
  LDFLAGS = LDFLAGS + ' -lbase -lfsudace -lncsuopt -lpsuade -lutilib';
  LDFLAGS = LDFLAGS + ' -lbose -lgss -lnewmat -lrandom -lutilities';
  LDFLAGS = LDFLAGS + ' -lcolin -linterfaces -lnewton -lsampling -lutils';
  LDFLAGS = LDFLAGS + ' -lconmin -ljega_fe -lnidr -lscolib';
  LDFLAGS = LDFLAGS + ' -lconstraints -ljega -lnkm -lsoga';
  LDFLAGS = LDFLAGS + ' -lcport -llhs -lopt';
  LDFLAGS = LDFLAGS + ' ' + DAKOTA_PATH + '/lib/libsparsegrid.a';
end

CFLAGS = '-ggdb -I' + DAKOTA_PATH + '/include -I' + TBX_GATEWAY_PATH;

CFLAGS = CFLAGS + ' -DHAVE_CONFIG_H -DDISABLE_DAKOTA_CONFIG_H -DDAKOTA_PLUGIN ';

CFLAGS = CFLAGS + ' -DBOOST_MULTI_INDEX_DISABLE_SERIALIZATION -DBOOST_DISABLE_ASSERTS -DHAVE_UNISTD_H -DHAVE_SYSTEM';
CFLAGS = CFLAGS + ' -DHAVE_RPC_XDR_H -DHAVE_RPC_TYPES_H';
CFLAGS = CFLAGS + ' -DHAVE_WORKING_FORK -DHAVE_SYS_WAIT_H -DHAVE_USLEEP -DHAVE_PECOS';
CFLAGS = CFLAGS + ' -DHAVE_SURFPACK -DDAKOTA_COLINY -DDAKOTA_UTILIB -DHAVE_CONMIN';
CFLAGS = CFLAGS + ' -DDAKOTA_DDACE -DHAVE_FSUDACE -DHAVE_JEGA -DHAVE_NCSU -DHAVE_NL2SOL';
CFLAGS = CFLAGS + ' -DHAVE_OPTPP -DDAKOTA_OPTPP -DDAKOTA_NEWMAT -DHAVE_PSUADE -DHAVE_AMPL';

Files = ["sci_dakota_filename.cpp","model_interface_plugins.cpp","SciSerialDirectApplicInterface.cpp"];

Table = ["dakota_filename",  "sci_dakota_filename";];
	

tbx_build_gateway("sci_dakota_cpp", ..
                  Table, ..
                  Files, ..
                  TBX_GATEWAY_PATH, ..
                  '', ..
                  LDFLAGS, ..
                  CFLAGS);

clear tbx_build_gateway CFLAGS LDFLAGS Files Table;
