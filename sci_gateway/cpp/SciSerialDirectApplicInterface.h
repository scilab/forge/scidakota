#ifndef SCI_SERIAL_DIRECT_APPLIC_INTERFACE_H
#define SCI_SERIAL_DIRECT_APPLIC_INTERFACE_H

#include <DirectApplicInterface.H>

class SciDirectApplicInterface: public Dakota::DirectApplicInterface
{
public:

  //
  //- Heading: Constructor and destructor
  //

  /// constructor
  SciDirectApplicInterface(const Dakota::ProblemDescDB& problem_db);
  /// destructor
  ~SciDirectApplicInterface();

protected:

  //
  //- Heading: Virtual function redefinitions
  //

  // execute the input filter portion of a direct evaluation invocation
  //int derived_map_if(const Dakota::String& if_name);

  /// execute an analysis code portion of a direct evaluation invocation
  int derived_map_ac(const Dakota::String& ac_name);

  // execute the output filter portion of a direct evaluation invocation
  //int derived_map_of(const Dakota::String& of_name);

private:
  //
  //- Heading: Data
  //
};


inline SciDirectApplicInterface::~SciDirectApplicInterface()
{ /* Virtual destructor handles referenceCount at Interface level. */ }
#endif
