Toolbox Scidakota 1.0
Authors : Yann Chapalain / Yann Collette
****************************************

SCIDAKOTA TOOLBOX
-----------------

The Scidakota toolbox link two softwares : Dakota and Scilab.
Dakota is a toolkit with lot of mathematic methods, including
optimization methods. It is those methods which are used in 
Scidakota. Indeed, the optimization methods of Dakota are very
efficient. Scidakota permits to offer a portable solution for
the Scilab users who are insteresting by those Dakota skills.


SCIDAKOTA FUNCTION
------------------

The function of this toolbox is called dakota_filename. It
permits to run an optimization in Scilab when a Dakota
configuration file is precised in parameters.

For Rosenbrock example :

Mlist_result = dakota_filename('rosenbrock.in')


HOW USED SCIDAKOTA
------------------

In Scilab, there is a builder file to compile the toolboxes.
It is the same thing for Scidakota. The user must execute the
script builder.sce (exec builder.sce) to compile this toolbox.
After, he can load the toolbox functions with the specific
script loader.sce (exec loader.sce). Now, the user can use the
Scidakota function : dakota_filename(dakota_config_file.in).


LICENSE
-------




